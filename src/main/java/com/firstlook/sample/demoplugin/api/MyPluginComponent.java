package com.firstlook.sample.demoplugin.api;

public interface MyPluginComponent
{
    String getName();
}