package com.firstlook.sample.jira.webwork;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
//import com.atlassian.sal.api.user.UserKey;
import com.google.common.collect.Maps;
import com.google.common.collect.Lists;

import com.firstlook.sample.jira.entity.ao.CustomerEntity;
import com.firstlook.sample.jira.service.AOCustomerMapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.Enumeration;
import net.java.ao.Query;

import java.util.ArrayList; 
import java.util.List;
import java.util.*;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
//import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 

import com.atlassian.sal.api.user.UserManager;
//import com.example.tutorial.plugins.ServiceNowClient;

import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;
import java.util.Set;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;


import com.example.tutorial.plugins.admin.controller.api.Table;
import com.example.tutorial.plugins.admin.controller.impl.SNTables;
import com.example.tutorial.plugins.admin.controller.api.Column;
import com.example.tutorial.plugins.admin.controller.impl.SNColumns;
import com.example.tutorial.plugins.admin.controller.impl.NameValueFactory;
import com.example.tutorial.plugins.admin.controller.api.NameValueMapper;
//import com.example.tutorial.plugins.admin.util.FieldCollectionsUtils;
import com.example.tutorial.plugins.exceptions.*;


@Named("CustomerActionsAction")
public class CustomerActionsAction extends JiraWebActionSupport
{

    private static final String PLUGIN_STORAGE_KEY = "com.firstlook.sample.jira.webwork";
    private static final Logger log;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final RequestFactory requestFactory; 
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final ProjectManager projectManager;



    static
    {
        log = LoggerFactory.getLogger(CustomerActionsAction.class);
    }


    private AOCustomerMapping aocustomermapping;
    private CustomerEntity cust_entity;




    @Inject
    public CustomerActionsAction(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,AOCustomerMapping aocustomermapping,ProjectManager projectManager)
    {
        super();
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.aocustomermapping = aocustomermapping ;
        this.projectManager = projectManager;         
    }

    @Override
    protected void doValidation()
    {
        log.info("Entering doValidation");
        for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
        String n = (String) e.nextElement();
        String[] vals = request.getParameterValues(n);
        log.debug("name " + n + ": " + vals[0]);
        
        // if (currentIssue == null) {
        //     log.error("The local variable didn't get set");
        //     return;
        //     }
        }
    }


    @Override
    public String execute() throws Exception 
    {

        return super.execute(); //returns SUCCESS
    }


    @Override
    public String doDefault() throws Exception 
    {
        getAllCustomerEntity();
        return INPUT;
    }

    public String doUpdateCustomerDefault() throws Exception 
    {
        // int param = request.getParameter("sc_id");
        // getCustomerById(param);
        return INPUT;
    }

     public String doUpdateCustomerEntitys() throws Exception
        { 
            return INPUT;
        }

    public List<NameValueMapper> getAllCustomerEntity()
    {
        try
        {
            Iterable<CustomerEntity> customervariables = aocustomermapping.allCustomerEntity();
            List<NameValueMapper> nameValueList = new ArrayList<NameValueMapper>();
            for(CustomerEntity customervariable : customervariables) {
                nameValueList.add(new NameValueFactory(customervariable.getCustName(),String.valueOf(customervariable.getID())));
            }
            return nameValueList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    // public List<NameValueMapper> getCustomerById(int param)
    // {
    //     try
    //     {
    //         Iterable<CustomerEntity> customervariables = aocustomermapping.customerEntityById(param);
    //         List<NameValueMapper> nameValueList = new ArrayList<NameValueMapper>();
    //         for(CustomerEntity customervariable : customervariables) {
    //             nameValueList.add(new NameValueFactory(customervariable.getName(),String.valueOf(customervariable.getID())));
    //         }
    //         return nameValueList;
    //     }
    //     catch(Exception e)
    //     {
    //         throw new RuntimeException(e);
    //     }
    // }
    

     public CustomerEntity getCustomerEntity()
        {
           if(getCustomerEntityId() != null)
           {
            CustomerEntity customerentity = aocustomermapping.getCustomerEntity(Integer.parseInt(getCustomerEntityId()));
            if(customerentity != null)
            {
                return customerentity;
            } 
           }
           return null;
        }

        public String getCustomerEntityId()
        {
            return request.getParameter("sc_id");
        }

   
    public List<CustomerEntity> getCustomerEntitys()
    {
        try
        {
            Iterable<CustomerEntity> customervariables = aocustomermapping.allCustomerEntity();
            List<CustomerEntity> customerList = new ArrayList<CustomerEntity>();
            for(CustomerEntity customervariable : customervariables) {
                customerList.add(customervariable);
            }
            return customerList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

}
