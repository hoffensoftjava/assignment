package com.firstlook.sample.jira.controller.XmlElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;
import javax.xml.bind.annotation.*;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLCustomer
{
  @XmlElement public int id;
  @XmlElement public String custName;
  @XmlElement public String gender;
  @XmlElement public String custDesc;
  @XmlElement public String emailId;
  @XmlElement public List<XMLCustomer> mappings; 

}