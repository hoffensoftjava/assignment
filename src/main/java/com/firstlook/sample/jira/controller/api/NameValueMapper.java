package com.example.tutorial.plugins.admin.controller.api;

public interface NameValueMapper
{
	public String getName();
	public String getValue();
}