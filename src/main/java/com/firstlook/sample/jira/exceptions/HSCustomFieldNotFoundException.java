
package com.example.tutorial.plugins.exceptions;

public class HSCustomFieldNotFoundException extends RuntimeException {
        private final String field;

        public HSCustomFieldNotFoundException(final String message, final String field) {
            super(message);
            this.field = field;
        }

        public String getField() {
            return field;
        }
    }