AJS.toInit(function() 
{
  var baseUrl = AJS.params.baseURL;
  var tables;
  var fields;
  var customerId = AJS.$("#customerId").val();
  getCustomerList();
  
AJS.$("#save_customer").submit(function(e)
    {
      e.preventDefault();
      var customer = {};
      var endPointURL;
      var customerId = AJS.$("#customerId").val();
      var custName = AJS.$("#custName").val();
      var gender = AJS.$("#gender").val();
      var custDesc = AJS.$("#custDesc").val();
      var emailId = AJS.$("#emailId").val();   
      customer["custName"]=custName;
      customer["gender"]=gender;
      customer["custDesc"]=custDesc;
      customer["emailId"]=emailId;

      if(customerId)
        {
          customer["id"] = customerId;
          endPointURL = baseUrl + "/rest/restresource/1.0/config/customervalue/"+customerId;
        }
      else
        {
          endPointURL = baseUrl + "/rest/restresource/1.0/config/customervalues";
        }

      var jsonString = JSON.stringify(customer);
      AJS.$.ajax({
            url:  endPointURL,
            type: "POST",
            dataType: "json",
            headers: { 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
                },
                data:jsonString,
                success: function(tablesArray)
                {
                  
                  if(customerId)
                    {
                      alert("Scheme Updated Successfully!");
                    }
                    else
                    {
                      alert("Scheme Saved Successfully!");
                    }
                   
                      location.href = baseUrl + "/secure/CustomerAction!default.jspa";

                },

              });

    });

});




function getCustomerList(){
  var baseUrl = AJS.params.baseURL;
          AJS.$.ajax({ 
              type: 'GET', 
              url: baseUrl + "/rest/restresource/1.0/config/customervalues",
              data: { get_param: 'value' }, 
              success: function (data) 
              {
               var table = "<table class='aui aui-table-rowhover'><tr><th>Action</th><th>Name</th><th>Gender</th><th>Description</th><th>Email</th></tr>";
               
               for (var i=0;i<data.length;i++)
               {
                                
                table+="<tr><td><a href='http://localhost:2990/jira/secure/UpdateCustomer!UpdateCustomerDefault.jspa?sc_id="+data[i].id+"'>Edit</a>|<a href='javascript:void(0);' onclick='deleteCustomerEntity("+data[i].id+")'>Delete</a></td><td>"+data[i].custName+"</td><td>"+data[i].gender+"</td><td>"+data[i].custDesc+"</td><td>"+data[i].emailId+"</td></tr>";
               }

                table+="</table>";
               $('#cand').append(table);

              }
          }); 
        }


function deleteCustomerEntity(scid)
  {
    
    var baseUrl = AJS.params.baseURL;
    var answer = confirm("Deleting the scheme will delete all the associated FieldMapping as well as Project Mapping.Are you sure?");
      if (answer)
      {
        AJS.$.ajax({
        url: baseUrl + "/rest/restresource/1.0/config/customervalues/"+scid,
        type: "DELETE",
        dataType: "json",
        success: function(result)
         {
          
          location.href = baseUrl + "/secure/CustomerAction!default.jspa";

          }

        });
        
      }
  } 