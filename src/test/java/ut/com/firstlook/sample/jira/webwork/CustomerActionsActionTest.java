package ut.com.firstlook.sample.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.firstlook.sample.jira.webwork.CustomerActionsAction;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class CustomerActionsActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //CustomerActionsAction testClass = new CustomerActionsAction();

        throw new Exception("CustomerActionsAction has no tests!");

    }

}
