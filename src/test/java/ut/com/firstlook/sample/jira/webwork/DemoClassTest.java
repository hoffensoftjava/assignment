package ut.com.firstlook.sample.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.firstlook.sample.jira.webwork.DemoClass;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class DemoClassTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //DemoClass testClass = new DemoClass();

        throw new Exception("DemoClass has no tests!");

    }

}
