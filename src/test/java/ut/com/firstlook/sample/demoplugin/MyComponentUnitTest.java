package ut.com.firstlook.sample.demoplugin;

import org.junit.Test;
import com.firstlook.sample.demoplugin.api.MyPluginComponent;
import com.firstlook.sample.demoplugin.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}